Please follow [Fork a Repo, Compare Code, and Create a Pull Request](https://confluence.atlassian.com/display/BITBUCKET/Fork+a+Repo,+Compare+Code,+and+Create+a+Pull+Request) and do the following things.

1. Fork this repository.
2. In your forked repository, create a directory using your name and create an empty `README` file in the directory. For example, if a student's name is John Smith, then he should create a directory named `JohnSmith/` as shown in the root directory.
3. Push the changes to your forked repository.
4. Create a Pull Request.

